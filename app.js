function distance(first, second){
		if(!(Array.isArray(first) && Array.isArray(second))){
			throw Error("InvalidType")    
		}
		arrayCh1 = Array.from(new Set(first));
		arrayCh2 = Array.from(new Set(second));
		a = arrayCh1.filter(x => arrayCh2.includes(x))
		return (arrayCh1.length + arrayCh2.length - a.length * 2)
	}
	


module.exports.distance = distance